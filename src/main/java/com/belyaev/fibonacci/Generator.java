package com.belyaev.fibonacci;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Random;

/**
 * Created by dmitriybelyaev on 6/24/15.
 */
public class Generator {
    public static void main(String[] args) {
        try {
            Writer writer = new FileWriter("files/numbers.txt");
            Random rn = new Random();
            for (int i = 0; i < 10000000; i++) {
                writer.write(Math.abs(rn.nextInt()) % 1000000000 + "\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
