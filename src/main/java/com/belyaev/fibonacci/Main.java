package com.belyaev.fibonacci;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        List<Integer> list = new ArrayList<>();
        int max = 0;
        int cur;
        try (BufferedReader br = new BufferedReader(new FileReader("files/numbers.txt"))) {
            for(String line; (line = br.readLine()) != null; ) {
                cur = Integer.parseInt(line);
                if (max < cur) {
                    max = cur;
                }
                list.add(cur);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int[] fibonacciArray = generateFibonacciArray(max);
        System.out.println(System.currentTimeMillis() - startTime);
        long startCalcul = System.currentTimeMillis();
        int cores = 16;

        Thread[] threads = new Thread[cores];
        for (int i = 0; i < cores; i++) {
            threads[i] = new Thread(new Worker(fibonacciArray, list, cores, i));
            threads[i].start();
        }

        for (Thread thread: threads) {
            thread.join();
        }

        System.out.println(System.currentTimeMillis() - startCalcul);
    }

    public static int[] generateFibonacciArray(int max)
    {
        int quantity = (int) Math.floor((Math.log10(max) + 0.5 * Math.log10(5)) / Math.log10(0.5 * (1 + Math.sqrt(5)))) - 1;
        int[] result = new int[quantity];
        int cur = 2;
        int prev = 1;
        int tmp;
        result[0] = prev;
        result[1] = cur;
        int i = 2;
        while ((tmp = cur + prev) < max) {
            result[i++] = tmp;
            prev = cur;
            cur = tmp;
        }

        return result;
    }
}
